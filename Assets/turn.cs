﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class turn : MonoBehaviour
{
    float r1;
    float r2;
    float r3;

    void Start()
    {
        r1 = Random.Range(0, 0.7f);
        r2 = Random.Range(0, 0.7f);
        r3 = Random.Range(0, 0.7f);
    }

    void Update()
    {
        transform.Rotate(r1,r2,r3);
    }
}
