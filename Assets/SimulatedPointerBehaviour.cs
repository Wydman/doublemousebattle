﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SimulatedPointerBehaviour : MonoBehaviour
{
    Rigidbody rb;
    public GameObject BlueSideWin;
    public GameObject RedSideWin;
    public GameObject ReplayButton;

    public TextMeshProUGUI blueScore;
    public TextMeshProUGUI redScore;
    public TextMeshProUGUI highScore;
    private int highscoreValue = 0;

    private bool GameIsOver = false;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Cursor.visible = false;
    }

    void Update()
    {
        if (!GameIsOver)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            float inputAxis = Input.GetAxis("Mouse X");

            if (inputAxis < 0)
            {
                rb.AddForce(Vector3.left * 3 * inputAxis * Time.deltaTime);
            }
            if (inputAxis > 0)
            {
                rb.AddForce(Vector3.left * 3 * inputAxis * Time.deltaTime);
            }
        }
        else
            Cursor.lockState = CursorLockMode.None;
    }


    private void OnTriggerEnter(Collider other)
    {
        float score = Mathf.Round(rb.velocity.magnitude * 100);
        if (other.GetComponent<GameBorder>().side == "Right")
        {
            RedSideWin.SetActive(true);
            redScore.SetText("avec : " + score);
        }
        else
        {
            BlueSideWin.SetActive(true);
            blueScore.SetText("avec : " + score);
        }
        GameIsOver = true;
        transform.position = Vector3.zero;
        rb.velocity = Vector3.zero;
        ReplayButton.SetActive(true);
        Cursor.visible = true;
        if ((int)score > highscoreValue)
        {
            highScore.SetText("Highscore : " + score);
            highscoreValue = (int)score;
        }
    }


    public void Replay()
    {
        GameIsOver = false;
        BlueSideWin.SetActive(false);
        RedSideWin.SetActive(false);
        ReplayButton.SetActive(false);
        Cursor.visible = false;
        blueScore.SetText(" ");
        redScore.SetText("  ");
    }
}
